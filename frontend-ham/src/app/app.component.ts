import { Component } from '@angular/core';
import { Loading } from 'notiflix/build/notiflix-loading-aio';
import { Router } from '@angular/router';
import { DefaultService } from 'src/swagger';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'hoopr-audio-matcher';
  public videoDetails = {
    url: '',
  };
  errorReceived: boolean = false;
  responseReceived: boolean = false;
  matches: number = 0;
  uploadedAt: any;
  matchedTracks: any;
  public metaData: any;
  constructor(
    private router: Router,
    private recogniserService: DefaultService
  ) {}
  audioMatcher() {
    Loading.dots('Parsing the url...', {
      svgColor: '#ff3567',
    });

    setTimeout(() => Loading.change('Downloading the video file...'), 5000);
    setTimeout(() => Loading.change('Converting to mp3...'), 10000);
    setTimeout(() => Loading.change('Searching for matches...'), 15000);

    this.recogniserService.matchaudioMatchGet(this.videoDetails.url).subscribe(
      (response) => {
        console.log(response);
        if (response !== undefined || response !== null) {
          Loading.remove();
          this.responseReceived = true;
          this.errorReceived = false;
          this.matches = response['No-of-matches'];
          this.matchedTracks = response['Matches'];
          this.metaData = response['Metadata'];
          this.uploadedAt = new Date(
            response['Metadata']['publishedOn']
          ).toString();
          this.videoDetails.url = '';
        }
      },
      (err) => {
        console.log(err);
        this.responseReceived = true;
        this.errorReceived = true;
        Loading.remove();
      }
    );
  }
}
