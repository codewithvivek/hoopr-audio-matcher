from moviepy.editor import VideoFileClip

def convert_to_mp3(filename):
    clip = VideoFileClip(filename)
    clip.audio.write_audiofile("audioFile.mp3")
    clip.close()