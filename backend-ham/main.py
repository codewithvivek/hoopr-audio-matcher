from fastapi.middleware.cors import CORSMiddleware
import youtube_downloader
import file_converter
import os
import json
import sys
import requests
import re
from dejavu import Dejavu
from dejavu.logic.recognizer.file_recognizer import FileRecognizer

from argparse import RawTextHelpFormatter
from os.path import isdir
from typing import Union
from fastapi import FastAPI

app = FastAPI()

origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:4200",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

DEFAULT_CONFIG_FILE = "dejavu.cnf.SAMPLE"
YOUR_API_KEY = "AIzaSyBead7MTGPbzW3NT66yYqkBG-gQjdkYpzM";

def init(configpath):
    """
    Load config from a JSON file
    """
    try:
        with open(configpath) as f:
            config = json.load(f)
    except IOError as err:
        print(f"Cannot open configuration: {str(err)}. Exiting")
        sys.exit(1)

    # create a Dejavu instance
    return Dejavu(config)

@app.get("/")
def read_root():
    return "Welcome to Hoopr Audio Matcher"


@app.get("/match/")
def match_audio(video_url: str):
    id = re.split(r"/vi/|v=|/v/|youtu.be/|/embed/", video_url)
    id = re.split(r"&", id[1])[0] if id[1] != None else id[0];
    metadata = requests.get(f"https://www.googleapis.com/youtube/v3/videos?id={id}&key={YOUR_API_KEY}&part=snippet,contentDetails,statistics,status")
    djv = init(DEFAULT_CONFIG_FILE)
    print("Downloading the video...")
    filename = youtube_downloader.download_video(video_url, 'low')
    print("Converting to MP3...")
    file_converter.convert_to_mp3(filename)
    os.remove(filename)
    songs = djv.recognize(FileRecognizer, 'audioFile.mp3')
    print(songs)
    formatted_data = metadata.json()
    youtube_meta_data = {
        "name": formatted_data['items'][0]['snippet']['title'],
        "description": formatted_data['items'][0]['snippet']['description'],
        "publishedOn" : formatted_data['items'][0]['snippet']['publishedAt'],
        "stats": formatted_data['items'][0]['statistics']
    }
    response = {
        'No-of-matches': len(songs['results']),
        'Matches': list(map(lambda song: {
            'song_name': song['song_name'],
            'confidence': song['fingerprinted_confidence']
        },songs['results'])),
        'Metadata': youtube_meta_data,
    }
    # print('meta data is', youtube_meta_data)
    return response